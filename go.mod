module github.com/lu4p/ToRat_client

go 1.12

require (
	github.com/BurntSushi/xgb v0.0.0-20160522181843-27f122750802 // indirect
	github.com/EndFirstCorp/peekingReader v0.0.0-20171012052444-257fb6f1a1a6 // indirect
	github.com/cretz/bine v0.1.0
	github.com/davecheney/junk v0.0.0-20170418064243-155060ce6d56 // indirect
	github.com/lu4p/cat v0.0.0-20190304233712-475e18f62f24
	github.com/lu4p/shred v0.0.0-20190320004132-e392edbd29ea
	github.com/pierrre/archivefile v0.0.0-20170218184037-e2d100bc74f5
	github.com/vova616/screenshot v0.0.0-20190211115255-f9547e483e0a
	golang.org/x/crypto v0.0.0-20190404164418-38d8ce5564a5 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3 // indirect
)
